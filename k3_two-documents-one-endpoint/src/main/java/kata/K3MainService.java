package kata;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class K3MainService {

    private final CommentsRepository commentsRepository;
    private final AuthorsRepository authorsRepository;

    public K3MainService(CommentsRepository commentsRepository, AuthorsRepository authorsRepository) {
        this.commentsRepository = commentsRepository;
        this.authorsRepository = authorsRepository;
    }


    @GetMapping("comments/author/{id}/summary")
    public AuthorSummaryResponse findByAuthorById(@PathVariable("id") String authorId) {
        List<CommentTeaser> commentTeasers = commentsRepository
                .findByAuthorId(UUID.fromString(authorId))
                .stream()
                .map(this::toTeaser)
                .toList();

        Author author = authorsRepository
                .findById(UUID.fromString(authorId))
                .orElseThrow();

        return new AuthorSummaryResponse()
                .setAuthorId(author.getId())
                .setAuthorName(author.getName())
                .setCommentTeasers(commentTeasers);
    }

    private CommentTeaser toTeaser(Comment comment) {
        String teaserContent = comment
                .getContent()
                .substring(0, 10)
                + "…";
        return new CommentTeaser()
                .setContent(teaserContent)
                .setId(comment.getId());
    }
}
