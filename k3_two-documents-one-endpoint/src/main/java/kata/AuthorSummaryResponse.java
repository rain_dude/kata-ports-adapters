package kata;

import java.util.List;
import java.util.UUID;

public class AuthorSummaryResponse {

    private UUID authorId;
    private String authorName;
    private List<CommentTeaser> commentTeasers;

    public AuthorSummaryResponse() {
    }


    public UUID getAuthorId() {
        return authorId;
    }

    public AuthorSummaryResponse setAuthorId(UUID authorId) {
        this.authorId = authorId;
        return this;
    }

    public String getAuthorName() {
        return authorName;
    }

    public AuthorSummaryResponse setAuthorName(String authorName) {
        this.authorName = authorName;
        return this;
    }

    public List<CommentTeaser> getCommentTeasers() {
        return commentTeasers;
    }

    public AuthorSummaryResponse setCommentTeasers(List<CommentTeaser> commentTeasers) {
        this.commentTeasers = commentTeasers;
        return this;
    }


    @Override
    public String toString() {
        return "AuthorSummary{" +
                "authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                ", commentTeasers=" + commentTeasers +
                '}';
    }
}
