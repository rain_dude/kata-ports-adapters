package kata;

import java.util.UUID;

public class CommentTeaser {

    private UUID id;
    private String content;

    public CommentTeaser() {
    }


    public UUID getId() {
        return id;
    }

    public CommentTeaser setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getContent() {
        return content;
    }

    public CommentTeaser setContent(String content) {
        this.content = content;
        return this;
    }


    @Override
    public String toString() {
        return "Teaser{" +
                "id=" + id +
                ", content='" + content + '\'' +
                '}';
    }
}
