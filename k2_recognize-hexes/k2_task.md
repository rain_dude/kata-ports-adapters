# K2: find hexes

## The task

Analyse legacy code, propose parts that can be moved to separate hexes.

(Tips: you don't have to change the code, just write ideas down or draw separated modules / hexagons in drawing tool or on paper).
