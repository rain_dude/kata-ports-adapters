package kata;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
public class K2MainService {

    @Value("${target.url}")
    private String targetUrl;

    private final Map<Integer, String> cache = new HashMap<>();

    private final CommentsRepository commentsRepository;

    public K2MainService(CommentsRepository commentsRepository) {
        this.commentsRepository = commentsRepository;
    }


    @GetMapping("comments/report")
    public String getReport() throws IOException, URISyntaxException, InterruptedException {
        URI targetUri = acquireTargetUri();
        String article = acquiredArticle(targetUri);

        String title = "Rettid";

        String comments = commentsRepository
                .findAll()
                .toString();

        String htmlTemplate = loadHtmlTemplate();

        int hash = Objects
                .hash(targetUri, article, title, comments);
        if (cache.containsKey(hash)) {
            System.out.println("Loading cached response");
            return cache
                    .get(hash);
        } else {
            System.out.println("Combining new response");
            String newResponse = join(htmlTemplate, article, title, comments);
            cache
                    .put(hash, newResponse);
            return newResponse;
        }
    }

    private URI acquireTargetUri() throws URISyntaxException {
        return new URI(targetUrl);
    }

    private String acquiredArticle(URI targetUri) throws IOException, InterruptedException {
        HttpClient client = HttpClient
                .newBuilder()
                .build();
        HttpRequest httpRequest = HttpRequest
                .newBuilder(targetUri)
                .GET()
                .build();
        return client
                .send(httpRequest, HttpResponse.BodyHandlers.ofString())
                .body();
    }

    private String loadHtmlTemplate() throws IOException {
        File htmlTemplateFile = ResourceUtils
                .getFile("classpath:response.html");
        return FileUtils
                .readFileToString(htmlTemplateFile, StandardCharsets.UTF_8);
    }

    private String join(String htmlTemplate, String article, String title, String comments) throws IOException {
        htmlTemplate = htmlTemplate
                .replace("$title", title);
        htmlTemplate = htmlTemplate
                .replace("$article", article);
        htmlTemplate = htmlTemplate
                .replace("$comments", comments);
        htmlTemplate = htmlTemplate
                .replace("$timestamp", LocalDateTime.now().toString());
        return htmlTemplate;
    }
}
