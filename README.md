# Kata ports-adapters

Set of Katas focused on ports-adapters / hexagonal architecture.

## Business context

Imaging a service like Reddit or Disqus (content on the web and other people comments underneath).

Such system has knowledge about authors of comments and the comments. User can request addition of new comment, see other comments or request preview o comment authors history.

The purpose of the tasks is to analyse the code or also change id according to hexagonal architecture ideas.
