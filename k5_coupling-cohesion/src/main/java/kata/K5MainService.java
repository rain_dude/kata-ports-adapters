package kata;

import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
public class K5MainService {

    private final CommentsRepository commentsRepository;

    public K5MainService(CommentsRepository commentsRepository) {
        this.commentsRepository = commentsRepository;
    }


    @PostMapping("comments")
    public void add(@RequestBody Comment comment) {
        comment
                .setId(UUID.randomUUID());
        comment
                .setPostedAtTimestamp(LocalDateTime.now());

        commentsRepository
                .save(comment);
    }

    @GetMapping("comments/author/{id}")
    public List<Comment> findByAuthorById(@PathVariable("id") String authorId) {
        return commentsRepository
                .findByAuthorId(UUID.fromString(authorId))
                .stream()
                .peek(comment -> comment.setId(null))
                .toList();
    }

    @DeleteMapping("comments/{id}")
    public void deleteCommentById(@PathVariable("id") UUID commentId) {
        commentsRepository
                .deleteById(commentId);
    }
}
