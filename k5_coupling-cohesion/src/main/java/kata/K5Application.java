package kata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K5Application {

	public static void main(String[] args) {
		SpringApplication.run(K5Application.class, args);
	}
}
