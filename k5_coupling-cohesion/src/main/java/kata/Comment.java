package kata;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.util.UUID;

@Document("comments")
public class Comment {

    @Id
    private UUID id;

    @Field
    private UUID authorId;
    @Field
    private String content;
    @Field
    private LocalDateTime postedAtTimestamp;

    public Comment() {
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAuthorId() {
        return authorId;
    }

    public void setAuthorId(UUID authorId) {
        this.authorId = authorId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getPostedAtTimestamp() {
        return postedAtTimestamp;
    }

    public void setPostedAtTimestamp(LocalDateTime postedAtTimestamp) {
        this.postedAtTimestamp = postedAtTimestamp;
    }


    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", authorId=" + authorId +
                ", content='" + content + '\'' +
                ", postedAtTimestamp=" + postedAtTimestamp +
                '}';
    }
}
