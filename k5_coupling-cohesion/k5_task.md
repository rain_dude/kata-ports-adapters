# K5: when and why code should be changed

## The task

Think about legacy in project you found. You may find many ways that this code could refactored or architecture changed.

What are good reasons to change legacy code **or** leave it unchanged? Why?
