#!/bin/bash

# docker / podman
docker run -d \
        -p 27017:27017 \
        --name mongodb_kata \
        docker.io/mongo

# check if it works: docker exec -it mongodb_kata mongosh