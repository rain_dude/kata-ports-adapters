# K1: refactoring according to SOLID

## Business context

We got a simple component producing reports.

There are some loose plans for the next half year:

- business plans to change report shape,
- due to technical reasons reporting libs will have to change.

## The task

Prepare code for future demands according to SOLID rules.

(Tips: focus on letters S, I and D)
