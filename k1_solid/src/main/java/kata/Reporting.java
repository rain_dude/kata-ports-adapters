package kata;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

public class Reporting {

    private final CommentsRepository commentsRepository;
    private final AuthorsRepository authorsRepository;

    public Reporting(CommentsRepository commentsRepository, AuthorsRepository authorsRepository) {
        this.commentsRepository = commentsRepository;
        this.authorsRepository = authorsRepository;
    }


    public FileWriter getReport() {
        List<Comment> comments = commentsRepository
                .findAll();

        Map<UUID, Integer> commentsCountByAuthorIds = comments
                .stream()
                .collect(groupingBy(Comment::getAuthorId))
                .entrySet()
                .stream()
                .filter(entry -> !entry.getValue().isEmpty())
                .collect(toMap(
                        entry -> entry.getKey(),
                        entry -> entry.getValue().size()
                ));

        Map<UUID, String> namesByAuthorIds = authorsRepository
                .findAll()
                .stream()
                .filter(author -> commentsCountByAuthorIds.containsKey(author.getId()))
                .collect(toMap(
                        author -> author.getId(),
                        author -> author.getName()
                ));

        Map<String, String> values = namesByAuthorIds
                .entrySet()
                .stream()
                .collect(toMap(
                        entry -> entry.getValue(),
                        entry -> commentsCountByAuthorIds.get(entry.getKey()).toString()
                ));
        try {
            return createReportCsvFileWith(values);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public FileWriter createReportCsvFileWith(Map<String, String> values) throws IOException {
        String[] HEADERS = {"author", "comments"};
        FileWriter out = new FileWriter("book_new.csv");
        try (
                CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(HEADERS))) {
            values
                    .forEach((author, title) -> {
                        try {
                            printer
                                    .printRecord(author, title);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
            return out;
        }
    }
}
