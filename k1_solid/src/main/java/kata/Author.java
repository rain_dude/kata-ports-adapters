package kata;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document("authors")
public class Author {

    private UUID id;
    private String name;

    public Author() {
    }


    public UUID getId() {
        return id;
    }

    public Author setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Author setName(String name) {
        this.name = name;
        return this;
    }


    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
