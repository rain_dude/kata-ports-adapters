package kata;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CommentsRepository extends MongoRepository<Comment, UUID> {

    @Query
    List<Comment> findByAuthorId(UUID authorId);
}
