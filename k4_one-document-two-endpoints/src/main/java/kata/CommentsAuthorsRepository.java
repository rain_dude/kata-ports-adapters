package kata;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CommentsAuthorsRepository extends MongoRepository<CommentsAuthor, UUID> {
}
