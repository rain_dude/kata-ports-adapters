package kata;

import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.Comparator.comparing;

@RestController
public class K4MainService {

    private final CommentsAuthorsRepository commentsAuthorsRepository;

    public K4MainService(CommentsAuthorsRepository commentsAuthorsRepository) {
        this.commentsAuthorsRepository = commentsAuthorsRepository;
    }

    @PostMapping("comments")
    public void add(@RequestBody CommentPayload commentPayload) {
        Comment comment = new Comment()
                .setId(UUID.randomUUID())
                .setContent(commentPayload.getContent())
                .setPostedAtTimestamp(LocalDateTime.now());

        UUID authorId = commentPayload.getAuthorId() == null
                ? UUID.randomUUID()
                : commentPayload.getAuthorId();

        // check if such author already in DB
        Optional<CommentsAuthor> potentialAuthor = commentsAuthorsRepository
                .findById(authorId);

        CommentsAuthor commentsAuthor = potentialAuthor
                .map(existing -> {

                    // add comment to existing author document
                    List<Comment> comments = existing
                            .getComments();
                    comments
                            .add(comment);
                    System.out.println("found: " + existing);
                    return existing;
                })
                .orElseGet(() -> {

                    // create new author document with received comment
                    ArrayList<Comment> comments = new ArrayList<>();
                    comments
                            .add(comment);
                    CommentsAuthor justCreated = new CommentsAuthor()
                            .setId(authorId)
                            .setName(commentPayload.getAuthorName())
                            .setComments(comments);
                    System.out.println("created: " + justCreated);
                    return justCreated;
                });

        commentsAuthorsRepository
                .save(commentsAuthor);
    }

    @GetMapping("author/{id}")
    public AuthorResponse findAuthorById(@PathVariable("id") String authorId) {
        CommentsAuthor commentsAuthor = commentsAuthorsRepository
                .findById(UUID.fromString(authorId))
                .orElseThrow();
        Comment firstPostedComment = commentsAuthor
                .getComments()
                .stream()
                .min(comparing(Comment::getPostedAtTimestamp))
                .orElseThrow();
        return new AuthorResponse()
                .setName(commentsAuthor.getName())
                .setActiveSince(firstPostedComment.getPostedAtTimestamp());
    }

    @GetMapping("comments/author/{id}")
    public List<Comment> findCommentsByAuthorById(@PathVariable("id") String authorId) {
        return commentsAuthorsRepository
                .findById(UUID.fromString(authorId))
                .orElseThrow()
                .getComments();
    }
}
