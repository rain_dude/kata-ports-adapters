package kata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K4Application {

	public static void main(String[] args) {
		SpringApplication.run(K4Application.class, args);
	}
}
