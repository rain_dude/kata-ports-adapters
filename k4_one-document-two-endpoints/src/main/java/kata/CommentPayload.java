package kata;

import java.util.UUID;

public class CommentPayload {

    private UUID authorId;
    private String authorName;
    private String content;

    public CommentPayload() {
    }


    public UUID getAuthorId() {
        return authorId;
    }

    public CommentPayload setAuthorId(UUID authorId) {
        this.authorId = authorId;
        return this;
    }

    public String getAuthorName() {
        return authorName;
    }

    public CommentPayload setAuthorName(String authorName) {
        this.authorName = authorName;
        return this;
    }

    public String getContent() {
        return content;
    }

    public CommentPayload setContent(String content) {
        this.content = content;
        return this;
    }


    @Override
    public String toString() {
        return "CommentPayload{" +
                "authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
