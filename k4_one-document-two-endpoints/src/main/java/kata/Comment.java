package kata;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.util.UUID;

public class Comment {

    @Id
    private UUID id;

    @Field
    private String content;
    @Field
    private LocalDateTime postedAtTimestamp;

    public Comment() {
    }


    public UUID getId() {
        return id;
    }

    public Comment setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Comment setContent(String content) {
        this.content = content;
        return this;
    }

    public LocalDateTime getPostedAtTimestamp() {
        return postedAtTimestamp;
    }

    public Comment setPostedAtTimestamp(LocalDateTime postedAtTimestamp) {
        this.postedAtTimestamp = postedAtTimestamp;
        return this;
    }


    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", postedAtTimestamp=" + postedAtTimestamp +
                '}';
    }
}
