package kata;

import java.time.LocalDateTime;

public class AuthorResponse {

    private String name;
    private LocalDateTime activeSince;

    public AuthorResponse() {
    }


    public String getName() {
        return name;
    }

    public AuthorResponse setName(String name) {
        this.name = name;
        return this;
    }

    public LocalDateTime getActiveSince() {
        return activeSince;
    }

    public AuthorResponse setActiveSince(LocalDateTime activeSince) {
        this.activeSince = activeSince;
        return this;
    }


    @Override
    public String toString() {
        return "AuthorResponse{" +
                "name='" + name + '\'' +
                ", activeSince=" + activeSince +
                '}';
    }
}
