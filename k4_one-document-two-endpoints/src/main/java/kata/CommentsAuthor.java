package kata;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.UUID;

@Document("commentsAuthors")
public class CommentsAuthor {

    private UUID id;
    private String name;
    private List<Comment> comments;

    public CommentsAuthor() {
    }


    public UUID getId() {
        return id;
    }

    public CommentsAuthor setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CommentsAuthor setName(String name) {
        this.name = name;
        return this;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public CommentsAuthor setComments(List<Comment> comments) {
        this.comments = comments;
        return this;
    }


    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", comments=" + comments +
                '}';
    }
}
